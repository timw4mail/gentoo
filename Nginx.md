#Setting up nginx for PHP-fpm

## Use flags
Recommended USE flags for nginx - put these in `/etc/portage/package.use` rather than `/etc/make.conf` to make this easier to change if need be. Recommended nginx flags:

	http http-cache ipv6 pcre ssl pcre-jit

### Modules Variable
To specify the modules to compile for nginx, you can add a `NGINX_MODULES_HTTP` variable to the `make.conf` file. Some recommended flags are below:

	spdy gzip_static gunzip access auth_basic charset fastcgi gzip limit_conn limit_req
	proxy rewrite ssl headers_more flv mp4 upload_progress 


## Mod Pagespeed

Mod Pagespeed is a google module to enable various web page optimizations on the fly. To set up the module, see the [Nginx mod_pagespeed github page](https://github.com/pagespeed/ngx_pagespeed). Skip the installing nginx part for the moment.

To compile nginx with the new module, add the following variable to `make.conf`
	
	NGINX_ADD_MODULES="/path/to/mod_pagespeed"

You can then compile nginx with `emerge nginx`;

## Config

### FastCGI Setup

I recommend first duplicating `fastcgi.conf` in the `/etc/nginx/` folder as `php.conf`.

### Pathinfo Fix

Pathinfo isn't properly passed to php by default. To generally apply the fix, add this location block to the server blocks you want set up. The easiest setup is to use an included  file for each site. 

	location ~ \.php {
		
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		
		fastcgi_param	PATH_INFO	$fastcgi_path_info;
		fastcgi_param	PATH_TRANSLATED	$document_root$fastcgi_path_info;
		
	} 


### Sites
Each site should have a pattern like this

	server {
		listen 80;
		listen 443 ssl spdy;
		
		server_name ~^example\.com$;
		keepalive_timeout 70;
		
		ssl_certificate /path/to/ssl/certificate;
		ssl_certificate_key /path/to/generated/key;
		ssl_session_cache shared:SSL:10m;
		ssl_session_timeout 10m;
		ssl_ciphers ECDHE-RSA-AES256-SHA256:AES256-GCM-SHA256:RC4:HIGH:!MD5:!aNULL:!EDH;
		ssl_prefer_server_ciphers on;
		ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
		
		root /path/to/root/dir/;
		
		include "php.conf";
	}

For non-ssl sites, the second listen line, and each line prefixed with ssl can be omitted. 

#### SPDY
SPDY is essentially HTTP 2.0. It allows piplining different files into one request, and compresses HTTP headers making page loads much quicker. Firefox, Chrome, and Opera all support SPDY. The downside is that it requires SSL. For browsers that don't support SPDY, the server sends an HTTP request instead. 