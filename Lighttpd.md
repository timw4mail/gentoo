#Setting up Lighttpd for PHP-fpm

## Use flags

Recommended USE Flags for lighttpd - put these in `/etc/portage/package.use` rather than `/etc/make.conf` to make this easier to change if need be.

`fastcgi -php threads openssl`

To install, just do

`emerge lighttpd`


## Config

### FastCGI Setup

To use lighttpd with php-fpm, we'll need to modify `/etc/lighttpd/mod_fastcgi.conf`
to look like so:

	server.modules += ("mod_fastcgi")
	fastcgi.server = ( ".php" =>
		            ( "localhost" =>
			            (
							"host" 		=> 		"127.0.0.1",
							"port"      => 		"9000"
			            )
		            )
	            )

### Sites	
To make adding/editing websites easier, I recommend creating a `/etc/lighttpd/sites.conf` file, and including it in the `/etc/lighttpd/lighttpd.conf` file.

Each site can look something like this (this example has a rewrite for Codeigniter): 

	$HTTP["host"] =~ "^timshomepage\.net" {
		server.document-root = "timshomepage.net/web"
		url.rewrite-if-not-file = (
		 "^/(.*)$" => "/index.php/$1"
		)
	}
	
To proxy a site to a different port you'd have something like this: 

	$HTTP["host"] =~"(^|\.)nodejs\.timshomepage.net$" {
		proxy.server = ( "" =>
			( 
				(
					"host" => "127.0.0.1",
			 		 "port" => "8124"
			 	)
			)
		)
	}