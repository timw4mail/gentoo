#Setting up PHP

## Use Flags

In order for PHP to be most useful, it should have as many of the modules installed as will be used. I recommend installing it using the FPM sapi, which manages php session spawning. 

Here are some recommended flags:
	
	mysql mysqli utf8 threads -readline libssh2 -cgi fpm xcache postgres 
	mysqlnd reflection session simplexml sockets spl pdo mbstring sqlite3  
	soap spell firebird exif cjk sqlite mssql xpm tidy calendar xmlrpc doc
	

## PHP_TARGETS

The `PHP_TARGETS` setting in `/etc/make.conf` lets you select which versions of PHP to compile. If possible, I recommend always using the latest stable version. 

As of the time this is written, php 5.4 is the latest version, so I would add this line to `/etc/make.conf`

`PHP_TARGETS="php5-4"`

Please note that you can have multiple PHP_TARGETS:

`PHP_TARGETS="php5-3 php5-4"`

As of this writing, PHP compiles with clang reliably. 

## php.ini and php-fpm.conf

### php.ini

This file is going to be under `/etc/php/[sapi]-php[version]/php.ini`

So, if I'm running php 5.3, and I want to adjust cli settings, the file is `/etc/php/cli-php5.3/php.ini`

#### Mandatory settings

* `date.timezone` - set to your default timezone, for example, `America/Detroit`

#### Recommended settings
* `short_open_tag = On` - Allow short tags `<? and <?=`
* `expose_php = Off` - With this enabled, a pointless server header is sent out, and some magic urls are enabled. There's no reason to enable this. 

### php-fpm.conf

When compiling PHP with FPM, there is another config file, `php-fpm.conf`.

When starting php, it will throw an annoying message if you don't have the 
`pm.start_servers` setting configured. 

A good default is to set `pm.start_servers = 20`


