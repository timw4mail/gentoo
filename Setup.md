# General Setup

## Follow the Gentoo Handbook for installing Gentoo

[Gentoo Handbook](http://www.gentoo.org/doc/en/handbook/)

Generally, you can just use the amd64 version of Gentoo, as most servers now are 64-bit compatible

[amd64 handbook](http://www.gentoo.org/doc/en/handbook/handbook-amd64.xml)

Here are some general shortcuts/guidelines in the process.

* When getting the two tarballs for stage3 and portage, I recommend first retrieving the tarball, then retrieving portage, before extracting either. This way you only have to use links once. 
		
	1. run `links http://www.gentoo.org/main/en/mirrors.xml`
	2. Go to releases -> amd64 -> autobuilds and select an appropriate stage3. Hardened is recommended for a server. 
	3. Go up until you reach the first mirror directory. Go to the `snapshots` folder and download `portage-latest.tar.bz2`
	4. Extract the stage3 tarball : `tar xvjpf stage3-*.tar.bz2`
	5. Extract portage `tar xvjf /mnt/gentoo/portage-latest.tar.bz2 -C /mnt/gentoo/usr`

* In your `/etc/make.conf` file, I recommend the following for [compile options](http://www.gentoo.org/doc/en/handbook/handbook-amd64.xml?part=1&chap=5#doc_chap4):
	* `CFLAGS="-0s -pipe -march=native -pthread""`
	*  Set the MAKEOPTS property to 2x the number of processors/cores, eg, dual core would be `MAKEOPTS="-j4"`
	
* [Set your profile](http://www.gentoo.org/doc/en/handbook/handbook-amd64.xml?part=1&chap=6#doc_chap2) to a server profile, optionally, set it to a hardened server profile.
* Compile the kernel using [genkernel](http://www.gentoo.org/doc/en/handbook/handbook-amd64.xml?part=1&chap=7#doc_chap4), as it's less involved than manually configuring everything for the kernel. 
* Set the [hostname](http://www.gentoo.org/doc/en/handbook/handbook-amd64.xml?part=1&chap=8#doc_chap2) to the name of the server

	

### Install LLVM/Clang (Optional)

Clang is a faster compiler than the default GCC. It produces binary as fast, or faster than GCC. To set up clang, 

run `emerge llvm clang`

and add these to lines to the top of your `/etc/make.conf` file

	CC=/usr/bin/clang
	CXX=/usr/bin/clang++
	
If a package fails to compile with clang, you can comment out those lines, and recompile the package with GCC.
