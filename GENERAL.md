#General Gentoo maintainance

## Checking for updates
Sync with Gentoo's servers, and then see what updates there are:

`emerge --sync && emerge -pv --update world`

To install available updates:

`emerge --update world`

## Starting / Restarting Services

Gentoo services have init files in the `/etc/init.d/` directory.

* To start a service:
`/etc/init.d/[service] start`

* To restart a service:
`/etc/init.d/[service] restart`

* To stop a service:
`/etc/init.d/[service] stop`

* To start a service on boot:
`rc-update add [service] default`

* Common services

	`mysqld` - mysql
	
	`php-fpm` - php with fpm 
	
	`nginx` - nginx web server 
	
	`lighttpd` - lighttpd web server

## Installing software 

Generally, all there is to installing addtional software is 

`emerge [software]`

But sometimes you don't know the package name, or if it exists, so you want to search:

`emerge --search [software]`

And it's a good idea to see what packages will be installed, without actually installing it

`emerge -pv [software]`

* If the sofware you are trying to install is masked, you can add the software to the `package.keywords` file in `/etc/portage/`. If the file doesn't exist, you can create it.
Each package added to the file looks like this:
`[category]/[software] ~amd64` or if you are using a 32-bit version, `[category]/[software] ~x86`

* If the software doesn't have the `USE` settings it needs, for example if php doesn't have mysql support, you can add this to the `package.use` file in `/etc/portage` : 

`dev-lang/php mysql mysqli mysqlnd`

